package dev.rooftop.challenge.service;

public interface TokenService {
    String getToken();
}
