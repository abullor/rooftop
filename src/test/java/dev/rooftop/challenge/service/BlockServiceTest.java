package dev.rooftop.challenge.service;

import com.google.gson.Gson;
import dev.rooftop.challenge.domain.BlockInformation;
import dev.rooftop.challenge.service.impl.BlockServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

public class BlockServiceTest {
    private static final String GET_BLOCKS_URL = "_URL_/%s";

    @InjectMocks
    BlockServiceImpl blockService;

    @Mock
    RestTemplate restTemplate;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(blockService, "getBlocksUrl", GET_BLOCKS_URL);
    }

    @Test
    public void givenBlocksRequestWhenGetBlocksThenVerifyBlocks() {
        String token = "token";

        Gson gson = new Gson();

        BlockInformation blockInformation = gson.fromJson("{\n" +
                "    \"data\": [\n" +
                "        \"GYB8InXHDG2JmPm8DFU412DZjGfbwGDnFCiaFX6SmqtA88fnOjrv1IrjmQCeE25YQSgQCeBTxPGXrdbKva2VQ7HutSrcn850OUzv\",\n" +
                "        \"lHK2yetBvDOgTJ1m1NwlwUQnLfja4fIv8pPiHmoC4QVmHMQpiHv5IlXGREWzB4eB1vIoId6XAunsQa7cpqvDZMEVpnHCtGUa4RVA\",\n" +
                "        \"IdEJ3TnffW9COvJSy9IeVMDRSEOLelEKzOuIYIG7jnizAHD84ST54FZbLn2OqHnACgBjb7uDBCkVdpk2x1B7VbkkD3eNYxhD3ZuP\",\n" +
                "        \"zldodd2CTSHh94UbsExwDmYseFKsdka1G9jS2Mlg9lXsEeoUSbxAALah9olhUyfBAKd3QQq7Ut1bDeit3nqfrqbu0hcitCeXaEmB\"\n" +
                "    ],\n" +
                "    \"chunkSize\": 100,\n" +
                "    \"length\": 400\n" +
                "}", BlockInformation.class);

        Mockito.when(restTemplate.getForObject(String.format(GET_BLOCKS_URL, token), BlockInformation.class)).thenReturn(blockInformation);

        BlockInformation response = blockService.getBlocks(token);

        Assertions.assertEquals(blockInformation, response);

        Mockito.verify(restTemplate, Mockito.times(1)).getForObject(String.format(GET_BLOCKS_URL,
                token), BlockInformation.class);
    }
}