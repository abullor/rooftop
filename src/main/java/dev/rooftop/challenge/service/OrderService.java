package dev.rooftop.challenge.service;

public interface OrderService {
    String[] check(String[] blocks, String token);
}
