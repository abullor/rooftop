package dev.rooftop.challenge.runner;

import dev.rooftop.challenge.domain.BlockInformation;
import dev.rooftop.challenge.service.*;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
@RequiredArgsConstructor
public class RooftopChallengeRunner implements CommandLineRunner {
    private final BlockService blockService;
    private final CheckService checkService;
    private final OrderService orderService;
    private final SequentialityService sequentialityService;
    private final TokenService tokenService;

    @Override
    public void run(String... args) throws Exception {
        String token = tokenService.getToken();

        System.out.printf("El token obtenido es '%s'.%n", token);

        BlockInformation blockInformation = blockService.getBlocks(token);

        System.out.printf("\nSe obtuvieron %s bloques según el siguiente detalle:%n", blockInformation.getLength() / 100);

        Arrays.stream(blockInformation.getData()).sequential().forEach(System.out::println);

        String[] orderedBlocks = orderService.check(blockInformation.getData(), token);

        System.out.println("\nEl orden propuesto es:");

        Arrays.stream(orderedBlocks).sequential().forEach(System.out::println);

        System.out.printf("\nEl chequeo del resultado devolvió %s.%n",
                this.checkService.checkSolution(token, orderedBlocks));

        System.out.println("\nChequeando con caso de prueba:");
        String[] result = orderService.check(new String[]{"f319", "3720", "4e3e", "46ec", "c7df", "c1c7", "80fd", "c4ea"}, "b93ac073-eae4-405d-b4ef-bb82e0036a1d");

        // Esperamos que el resultado sea como este array
        String[] expected = new String[]{"f319", "46ec", "c1c7", "3720", "c7df", "c4ea", "4e3e", "80fd"};

        if (Arrays.equals(result, expected)) {
            System.out.println("Lo resolviste correctamente!");
        } else {
            System.out.println("El test falló. ¡Todavía puedes intentarlo!");
        }
    }
}
