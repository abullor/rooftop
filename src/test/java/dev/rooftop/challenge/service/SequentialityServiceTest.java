package dev.rooftop.challenge.service;

import com.google.gson.Gson;
import dev.rooftop.challenge.domain.ServiceResponse;
import dev.rooftop.challenge.service.impl.SequentialityServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

public class SequentialityServiceTest {
    private static final String ARE_SEQUENTIAL_URL = "_URL_/%s";

    @InjectMocks
    SequentialityServiceImpl sequentialityService;

    @Mock
    RestTemplate restTemplate;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(sequentialityService, "areSequentialUrl", ARE_SEQUENTIAL_URL);
        ReflectionTestUtils.setField(sequentialityService, "gson", new Gson());
    }

    @Test
    public void givenSequentialBlocksWhenAreSequentialThenVerifyTrue() {
        String token = "token";

        String block1 = "GYB8InXHDG2JmPm8DFU412DZjGfbwGDnFCiaFX6SmqtA88fnOjrv1IrjmQCeE25YQSgQCeBTxPGXrdbKva2VQ7HutSrcn850OUzv";
        String block2 = "lHK2yetBvDOgTJ1m1NwlwUQnLfja4fIv8pPiHmoC4QVmHMQpiHv5IlXGREWzB4eB1vIoId6XAunsQa7cpqvDZMEVpnHCtGUa4RVA";

        Gson gson = new Gson();

        ServiceResponse serviceResponse = gson.fromJson("{\n" +
                "   \"message\": true\n" +
                "}\n", ServiceResponse.class);

        String jsonRequest = "{\"blocks\":[\"GYB8InXHDG2JmPm8DFU412DZjGfbwGDnFCiaFX6SmqtA88fnOjrv1IrjmQCeE25YQSgQCeBTxPGXrdbKva2VQ7HutSrcn850OUzv\",\"lHK2yetBvDOgTJ1m1NwlwUQnLfja4fIv8pPiHmoC4QVmHMQpiHv5IlXGREWzB4eB1vIoId6XAunsQa7cpqvDZMEVpnHCtGUa4RVA\"]}";

        ResponseEntity<ServiceResponse> responseEntity = ResponseEntity.ok(serviceResponse);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        HttpEntity<String> entity = new HttpEntity<>(jsonRequest, headers);

        Mockito.when(restTemplate.postForEntity(String.format(ARE_SEQUENTIAL_URL, token),
                entity, ServiceResponse.class)).thenReturn(responseEntity);

        boolean response = sequentialityService.areSequential(token, block1, block2);

        Assertions.assertTrue(response);

        Mockito.verify(restTemplate, Mockito.times(1)).postForEntity(String.format(ARE_SEQUENTIAL_URL, token),
                entity, ServiceResponse.class);
    }

    @Test
    public void givenNonSequentialBlocksWhenAreSequentialThenVerifyFalse() {
        String token = "token";

        String block1 = "lHK2yetBvDOgTJ1m1NwlwUQnLfja4fIv8pPiHmoC4QVmHMQpiHv5IlXGREWzB4eB1vIoId6XAunsQa7cpqvDZMEVpnHCtGUa4RVA";
        String block2 = "GYB8InXHDG2JmPm8DFU412DZjGfbwGDnFCiaFX6SmqtA88fnOjrv1IrjmQCeE25YQSgQCeBTxPGXrdbKva2VQ7HutSrcn850OUzv";

        Gson gson = new Gson();

        ServiceResponse serviceResponse = gson.fromJson("{\n" +
                "   \"message\": false\n" +
                "}\n", ServiceResponse.class);

        String jsonRequest = "{\"blocks\":[\"lHK2yetBvDOgTJ1m1NwlwUQnLfja4fIv8pPiHmoC4QVmHMQpiHv5IlXGREWzB4eB1vIoId6XAunsQa7cpqvDZMEVpnHCtGUa4RVA\",\"GYB8InXHDG2JmPm8DFU412DZjGfbwGDnFCiaFX6SmqtA88fnOjrv1IrjmQCeE25YQSgQCeBTxPGXrdbKva2VQ7HutSrcn850OUzv\"]}";

        ResponseEntity<ServiceResponse> responseEntity = ResponseEntity.ok(serviceResponse);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        HttpEntity<String> entity = new HttpEntity<>(jsonRequest, headers);

        Mockito.when(restTemplate.postForEntity(String.format(ARE_SEQUENTIAL_URL, token),
                entity, ServiceResponse.class)).thenReturn(responseEntity);

        boolean response = sequentialityService.areSequential(token, block1, block2);

        Assertions.assertFalse(response);

        Mockito.verify(restTemplate, Mockito.times(1)).postForEntity(String.format(ARE_SEQUENTIAL_URL, token),
                entity, ServiceResponse.class);
    }
}