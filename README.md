# Rooftop challenge

## Descripción

El proyecto es una aplicación de consola con SpringBoot. Si se ejecuta la aplicación (comando mvn spring-boot:run), la ejecución consiste en invocar a los servicios reales, tomando la casilla de mail para la generación del token de la entrada getToken.email del archivo application.yaml. Todas las URLs están configurados en ese mismo archivo. Aparte de la invocación para esa casilla de mail, también se ejecuta la prueba con los bloques propuestos en https://github.com/RooftopAcademy/career-switch-challenge-test/blob/main/Test.java.

## Tests
Hay test unitarios realizados con JUnit y Mockito que sirven para testear sin hacer uso de los servicios. Se ejecutan con el comando mvn clean test. Si sólo interesa "check", se puede correr específicamente con el comando mvn clean test -Dtest=dev.rooftop.challenge.service.CheckServiceTest#givenValidSolutionWhenCheckSolutionThenVerifyTrue

No sé si son situaciones que pueden suceder, pero si la cantidad de bloques es 1 o 2, entiendo que puedo asumir que ya esta ordenado sin hacer nada extra (lo agrego como corner cases). Entiendo que no puede ser nulo.
