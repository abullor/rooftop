package dev.rooftop.challenge.service;

public interface SequentialityService {
    boolean areSequential(String token, String block1, String block2);
}
