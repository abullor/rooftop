package dev.rooftop.challenge.domain;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class CheckSequenceRequest {
    private final String[] blocks;
}
