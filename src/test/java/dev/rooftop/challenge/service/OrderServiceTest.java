package dev.rooftop.challenge.service;

import dev.rooftop.challenge.service.impl.OrderServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class OrderServiceTest {
    @InjectMocks
    OrderServiceImpl orderService;

    @Mock
    SequentialityService sequentialityService;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void givenBlocksWhenCheckThenVerifyAnswer() {
        String token = "token";

        String block1 = "GYB8InXHDG2JmPm8DFU412DZjGfbwGDnFCiaFX6SmqtA88fnOjrv1IrjmQCeE25YQSgQCeBTxPGXrdbKva2VQ7HutSrcn850OUzv";
        String block2 = "lHK2yetBvDOgTJ1m1NwlwUQnLfja4fIv8pPiHmoC4QVmHMQpiHv5IlXGREWzB4eB1vIoId6XAunsQa7cpqvDZMEVpnHCtGUa4RVA";
        String block3 = "IdEJ3TnffW9COvJSy9IeVMDRSEOLelEKzOuIYIG7jnizAHD84ST54FZbLn2OqHnACgBjb7uDBCkVdpk2x1B7VbkkD3eNYxhD3ZuP";
        String block4 = "zldodd2CTSHh94UbsExwDmYseFKsdka1G9jS2Mlg9lXsEeoUSbxAALah9olhUyfBAKd3QQq7Ut1bDeit3nqfrqbu0hcitCeXaEmB";


        String[] blocks = new String[] {block1, block2, block3, block4};

        /*
        The order is:
        block1
        block3
        block4
        block2

        GYB8InXHDG2JmPm8DFU412DZjGfbwGDnFCiaFX6SmqtA88fnOjrv1IrjmQCeE25YQSgQCeBTxPGXrdbKva2VQ7HutSrcn850OUzv
        + IdEJ3TnffW9COvJSy9IeVMDRSEOLelEKzOuIYIG7jnizAHD84ST54FZbLn2OqHnACgBjb7uDBCkVdpk2x1B7VbkkD3eNYxhD3ZuP
        + zldodd2CTSHh94UbsExwDmYseFKsdka1G9jS2Mlg9lXsEeoUSbxAALah9olhUyfBAKd3QQq7Ut1bDeit3nqfrqbu0hcitCeXaEmB
        + lHK2yetBvDOgTJ1m1NwlwUQnLfja4fIv8pPiHmoC4QVmHMQpiHv5IlXGREWzB4eB1vIoId6XAunsQa7cpqvDZMEVpnHCtGUa4RVA
         */

        String ordered = "GYB8InXHDG2JmPm8DFU412DZjGfbwGDnFCiaFX6SmqtA88fnOjrv1IrjmQCeE25YQSgQCeBTxPGXrdbKva2VQ7HutSrcn850OUzv" +
                "IdEJ3TnffW9COvJSy9IeVMDRSEOLelEKzOuIYIG7jnizAHD84ST54FZbLn2OqHnACgBjb7uDBCkVdpk2x1B7VbkkD3eNYxhD3ZuP" +
                "zldodd2CTSHh94UbsExwDmYseFKsdka1G9jS2Mlg9lXsEeoUSbxAALah9olhUyfBAKd3QQq7Ut1bDeit3nqfrqbu0hcitCeXaEmB" +
                "lHK2yetBvDOgTJ1m1NwlwUQnLfja4fIv8pPiHmoC4QVmHMQpiHv5IlXGREWzB4eB1vIoId6XAunsQa7cpqvDZMEVpnHCtGUa4RVA";

        Mockito.when(sequentialityService.areSequential(token, block1, block2)).thenReturn(false);
        Mockito.when(sequentialityService.areSequential(token, block1, block3)).thenReturn(true);
        Mockito.when(sequentialityService.areSequential(token, block1, block4)).thenReturn(false);

        Mockito.when(sequentialityService.areSequential(token, block2, block1)).thenReturn(false);
        Mockito.when(sequentialityService.areSequential(token, block2, block3)).thenReturn(false);
        Mockito.when(sequentialityService.areSequential(token, block2, block4)).thenReturn(false);

        Mockito.when(sequentialityService.areSequential(token, block3, block1)).thenReturn(false);
        Mockito.when(sequentialityService.areSequential(token, block3, block2)).thenReturn(false);
        Mockito.when(sequentialityService.areSequential(token, block3, block4)).thenReturn(true);

        Mockito.when(sequentialityService.areSequential(token, block4, block1)).thenReturn(false);
        Mockito.when(sequentialityService.areSequential(token, block4, block2)).thenReturn(true);
        Mockito.when(sequentialityService.areSequential(token, block4, block3)).thenReturn(false);

        String[] response = orderService.check(blocks, token);

        Assertions.assertEquals(ordered, String.join("", response));
    }

    @Test
    public void givenOneBlockWhenCheckThenVerifyCornerCase() {
        String token = "token";

        String block1 = "GYB8InXHDG2JmPm8DFU412DZjGfbwGDnFCiaFX6SmqtA88fnOjrv1IrjmQCeE25YQSgQCeBTxPGXrdbKva2VQ7HutSrcn850OUzv";

        String[] blocks = new String[] {block1};

        /*
        The order is:
        block1

        GYB8InXHDG2JmPm8DFU412DZjGfbwGDnFCiaFX6SmqtA88fnOjrv1IrjmQCeE25YQSgQCeBTxPGXrdbKva2VQ7HutSrcn850OUzv
        */

        String ordered = "GYB8InXHDG2JmPm8DFU412DZjGfbwGDnFCiaFX6SmqtA88fnOjrv1IrjmQCeE25YQSgQCeBTxPGXrdbKva2VQ7HutSrcn850OUzv";

        String[] response = orderService.check(blocks, token);

        Assertions.assertEquals(ordered, String.join("", response));
    }

    @Test
    public void givenTwoBlocksWhenCheckThenVerifyCornerCase() {
        String token = "token";

        String block1 = "GYB8InXHDG2JmPm8DFU412DZjGfbwGDnFCiaFX6SmqtA88fnOjrv1IrjmQCeE25YQSgQCeBTxPGXrdbKva2VQ7HutSrcn850OUzv";
        String block2 = "lHK2yetBvDOgTJ1m1NwlwUQnLfja4fIv8pPiHmoC4QVmHMQpiHv5IlXGREWzB4eB1vIoId6XAunsQa7cpqvDZMEVpnHCtGUa4RVA";

        String[] blocks = new String[] {block1, block2};

        /*
        The order is:
        block1
        block2

        GYB8InXHDG2JmPm8DFU412DZjGfbwGDnFCiaFX6SmqtA88fnOjrv1IrjmQCeE25YQSgQCeBTxPGXrdbKva2VQ7HutSrcn850OUzv
        + lHK2yetBvDOgTJ1m1NwlwUQnLfja4fIv8pPiHmoC4QVmHMQpiHv5IlXGREWzB4eB1vIoId6XAunsQa7cpqvDZMEVpnHCtGUa4RVA
         */

        String ordered = "GYB8InXHDG2JmPm8DFU412DZjGfbwGDnFCiaFX6SmqtA88fnOjrv1IrjmQCeE25YQSgQCeBTxPGXrdbKva2VQ7HutSrcn850OUzv" +
                "lHK2yetBvDOgTJ1m1NwlwUQnLfja4fIv8pPiHmoC4QVmHMQpiHv5IlXGREWzB4eB1vIoId6XAunsQa7cpqvDZMEVpnHCtGUa4RVA";

        String[] response = orderService.check(blocks, token);

        Assertions.assertEquals(ordered, String.join("", response));
    }
}