package dev.rooftop.challenge.service.impl;

import com.google.gson.Gson;
import dev.rooftop.challenge.domain.CheckSequenceRequest;
import dev.rooftop.challenge.domain.ServiceResponse;
import dev.rooftop.challenge.service.SequentialityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@Service
public class SequentialityServiceImpl implements SequentialityService {
    private final RestTemplate restTemplate;
    private final String areSequentialUrl;
    private final Gson gson;

    @Autowired
    public SequentialityServiceImpl(RestTemplate restTemplate,
                                    @Value("${areSequential.url}") String areSequentialUrl,
                                    Gson gson) {
        this.restTemplate = restTemplate;
        this.areSequentialUrl = areSequentialUrl;
        this.gson = gson;
    }

    @Override
    public boolean areSequential(String token, String block1, String block2) {
        CheckSequenceRequest request = new CheckSequenceRequest(new String[] {block1, block2});

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        HttpEntity<String> entity = new HttpEntity<>(gson.toJson(request), headers);

        ResponseEntity<ServiceResponse> response = restTemplate.postForEntity(String.format(this.areSequentialUrl, token),
                entity, ServiceResponse.class);

        return response.getBody().isMessage();
    }
}
