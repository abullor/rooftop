package dev.rooftop.challenge.service;

public interface CheckService {
    boolean checkSolution(String token, String[] blocks);
}
