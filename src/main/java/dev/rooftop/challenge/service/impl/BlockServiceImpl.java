package dev.rooftop.challenge.service.impl;

import dev.rooftop.challenge.domain.BlockInformation;
import dev.rooftop.challenge.service.BlockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class BlockServiceImpl implements BlockService {
    private final RestTemplate restTemplate;
    private final String getBlocksUrl;

    @Autowired
    public BlockServiceImpl(RestTemplate restTemplate,
                            @Value("${getBlocks.url}") String getBlocksUrl) {
        this.restTemplate = restTemplate;
        this.getBlocksUrl = getBlocksUrl;
    }

    @Override
    public BlockInformation getBlocks(String token) {
        return this.restTemplate.getForObject(String.format(this.getBlocksUrl, token),
                BlockInformation.class);
    }
}
