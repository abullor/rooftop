package dev.rooftop.challenge;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class RooftopChallengeApplication {
	public static void main(String[] args) {
		SpringApplication.run(RooftopChallengeApplication.class, args);
	}
}
