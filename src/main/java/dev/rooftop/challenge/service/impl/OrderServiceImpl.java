package dev.rooftop.challenge.service.impl;

import dev.rooftop.challenge.service.OrderService;
import dev.rooftop.challenge.service.SequentialityService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {
    private final SequentialityService sequentialityService;

    public String[] check(String[] blocks, String token) {
        if (blocks.length <= 2) {
            return blocks;
        }

        List<String> response = new ArrayList<>();

        String current = blocks[0];

        response.add(current);

        Set<String> blocksToCheck = new HashSet<>(Arrays.asList(Arrays.copyOfRange(blocks, 1, blocks.length)));

        while (blocksToCheck.size() > 1) {
            boolean found = false;

            for (Iterator<String> it = blocksToCheck.iterator(); it.hasNext() && !found; ) {
                String block = it.next();

                if (this.sequentialityService.areSequential(token, current, block)) {
                    current = block;
                    found = true;
                }
            }

            blocksToCheck.remove(current);
            response.add(current);
        }

        response.add(blocksToCheck.iterator().next());

        return response.toArray(new String[0]);
    }
}