package dev.rooftop.challenge.domain;

import lombok.Data;

@Data
public class BlockInformation {
    private String[] data;
    private Integer chunkSize;
    private Integer length;
}
