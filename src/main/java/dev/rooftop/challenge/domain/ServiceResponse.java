package dev.rooftop.challenge.domain;

import lombok.Data;

@Data
public class ServiceResponse {
    private boolean message;
}
