package dev.rooftop.challenge.service;

import dev.rooftop.challenge.domain.BlockInformation;

public interface BlockService {
    BlockInformation getBlocks(String token);
}
