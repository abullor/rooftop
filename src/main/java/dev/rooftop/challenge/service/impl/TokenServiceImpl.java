package dev.rooftop.challenge.service.impl;

import dev.rooftop.challenge.domain.TokenResponse;
import dev.rooftop.challenge.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class TokenServiceImpl implements TokenService {
    private final RestTemplate restTemplate;
    private final String getTokenUrl;
    private final String email;

    @Autowired
    public TokenServiceImpl(RestTemplate restTemplate, @Value("${getToken.url}") String getTokenUrl,
                            @Value("${getToken.email}") String email) {
        this.restTemplate = restTemplate;
        this.getTokenUrl = getTokenUrl;
        this.email = email;
    }

    @Override
    public String getToken() {
        return this.restTemplate.getForObject(String.format(this.getTokenUrl, email), TokenResponse.class).getToken();
    }
}
