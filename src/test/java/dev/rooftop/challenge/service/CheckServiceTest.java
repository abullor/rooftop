package dev.rooftop.challenge.service;

import com.google.gson.Gson;
import dev.rooftop.challenge.domain.ServiceResponse;
import dev.rooftop.challenge.service.impl.CheckServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

public class CheckServiceTest {
    private static final String CHECK_SOLUTION_URL = "_URL_/%s";

    @InjectMocks
    CheckServiceImpl checkService;

    @Mock
    RestTemplate restTemplate;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(checkService, "checkSolutionUrl", CHECK_SOLUTION_URL);
        ReflectionTestUtils.setField(checkService, "gson", new Gson());
    }

    @Test
    public void givenValidSolutionWhenCheckSolutionThenVerifyTrue() {
        String token = "token";

        String block1 = "GYB8InXHDG2JmPm8DFU412DZjGfbwGDnFCiaFX6SmqtA88fnOjrv1IrjmQCeE25YQSgQCeBTxPGXrdbKva2VQ7HutSrcn850OUzv";
        String block2 = "lHK2yetBvDOgTJ1m1NwlwUQnLfja4fIv8pPiHmoC4QVmHMQpiHv5IlXGREWzB4eB1vIoId6XAunsQa7cpqvDZMEVpnHCtGUa4RVA";
        String block3 = "IdEJ3TnffW9COvJSy9IeVMDRSEOLelEKzOuIYIG7jnizAHD84ST54FZbLn2OqHnACgBjb7uDBCkVdpk2x1B7VbkkD3eNYxhD3ZuP";
        String block4 = "zldodd2CTSHh94UbsExwDmYseFKsdka1G9jS2Mlg9lXsEeoUSbxAALah9olhUyfBAKd3QQq7Ut1bDeit3nqfrqbu0hcitCeXaEmB";

        String[] blocks = new String[] {block1, block3, block4, block2};

        Gson gson = new Gson();

        ServiceResponse serviceResponse = gson.fromJson("{\n" +
                "   \"message\": true\n" +
                "}\n", ServiceResponse.class);

        String jsonRequest = "{\"encoded\":\"" + String.join("", blocks) + "\"}";

        ResponseEntity<ServiceResponse> responseEntity = ResponseEntity.ok(serviceResponse);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        HttpEntity<String> entity = new HttpEntity<>(jsonRequest, headers);

        Mockito.when(restTemplate.postForEntity(String.format(CHECK_SOLUTION_URL, token),
                entity, ServiceResponse.class)).thenReturn(responseEntity);

        boolean response = checkService.checkSolution(token, blocks);

        Assertions.assertTrue(response);

        Mockito.verify(restTemplate, Mockito.times(1)).postForEntity(String.format(CHECK_SOLUTION_URL, token),
                entity, ServiceResponse.class);
    }

    @Test
    public void givenNonValidSolutionWhenCheckSolutionThenVerifyFalse() {
        String token = "token";

        String block1 = "GYB8InXHDG2JmPm8DFU412DZjGfbwGDnFCiaFX6SmqtA88fnOjrv1IrjmQCeE25YQSgQCeBTxPGXrdbKva2VQ7HutSrcn850OUzv";
        String block2 = "lHK2yetBvDOgTJ1m1NwlwUQnLfja4fIv8pPiHmoC4QVmHMQpiHv5IlXGREWzB4eB1vIoId6XAunsQa7cpqvDZMEVpnHCtGUa4RVA";
        String block3 = "IdEJ3TnffW9COvJSy9IeVMDRSEOLelEKzOuIYIG7jnizAHD84ST54FZbLn2OqHnACgBjb7uDBCkVdpk2x1B7VbkkD3eNYxhD3ZuP";
        String block4 = "zldodd2CTSHh94UbsExwDmYseFKsdka1G9jS2Mlg9lXsEeoUSbxAALah9olhUyfBAKd3QQq7Ut1bDeit3nqfrqbu0hcitCeXaEmB";

        String[] blocks = new String[] {block1, block3, block4, block2};

        Gson gson = new Gson();

        ServiceResponse serviceResponse = gson.fromJson("{\n" +
                "   \"message\": false\n" +
                "}\n", ServiceResponse.class);

        String jsonRequest = "{\"encoded\":\"" + String.join("", blocks) + "\"}";

        ResponseEntity<ServiceResponse> responseEntity = ResponseEntity.ok(serviceResponse);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        HttpEntity<String> entity = new HttpEntity<>(jsonRequest, headers);

        Mockito.when(restTemplate.postForEntity(String.format(CHECK_SOLUTION_URL, token),
                entity, ServiceResponse.class)).thenReturn(responseEntity);

        boolean response = checkService.checkSolution(token, blocks);

        Assertions.assertFalse(response);

        Mockito.verify(restTemplate, Mockito.times(1)).postForEntity(String.format(CHECK_SOLUTION_URL, token),
                entity, ServiceResponse.class);
    }
}