package dev.rooftop.challenge.service;

import dev.rooftop.challenge.domain.TokenResponse;
import dev.rooftop.challenge.service.impl.TokenServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

public class TokenServiceTest {
    private static final String GET_TOKEN_URL = "_URL_/%s";
    private static final String GET_TOKEN_EMAIL = "test@gmail.com";

    @InjectMocks
    TokenServiceImpl tokenService;

    @Mock
    RestTemplate restTemplate;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(tokenService, "getTokenUrl", GET_TOKEN_URL);
        ReflectionTestUtils.setField(tokenService, "email", GET_TOKEN_EMAIL);
    }

    @Test
    public void givenEmailWhenGetTokenThenVerifyToken() {
        String token = "token";

        Mockito.when(restTemplate.getForObject(String.format(GET_TOKEN_URL, GET_TOKEN_EMAIL), TokenResponse.class)).thenReturn(new TokenResponse(token));

        String response = tokenService.getToken();

        Assertions.assertEquals(token, response);

        Mockito.verify(restTemplate, Mockito.times(1)).getForObject(String.format(GET_TOKEN_URL, GET_TOKEN_EMAIL),
                TokenResponse.class);
    }
}