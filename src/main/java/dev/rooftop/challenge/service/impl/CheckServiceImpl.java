package dev.rooftop.challenge.service.impl;

import com.google.gson.Gson;
import dev.rooftop.challenge.domain.CheckSolutionRequest;
import dev.rooftop.challenge.domain.ServiceResponse;
import dev.rooftop.challenge.service.CheckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@Service
public class CheckServiceImpl implements CheckService {
    private final RestTemplate restTemplate;
    private final String checkSolutionUrl;
    private final Gson gson;

    @Autowired
    public CheckServiceImpl(RestTemplate restTemplate,
                            @Value("${checkSolution.url}") String checkSolutionUrl,
                            Gson gson) {
        this.restTemplate = restTemplate;
        this.checkSolutionUrl = checkSolutionUrl;
        this.gson = gson;
    }

    @Override
    public boolean checkSolution(String token, String[] blocks) {
        CheckSolutionRequest request = new CheckSolutionRequest(String.join("", blocks));

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        HttpEntity<String> entity = new HttpEntity<>(gson.toJson(request), headers);

        ResponseEntity<ServiceResponse> response = restTemplate.postForEntity(String.format(this.checkSolutionUrl, token),
                entity, ServiceResponse.class);

        return response.getBody().isMessage();
    }
}
